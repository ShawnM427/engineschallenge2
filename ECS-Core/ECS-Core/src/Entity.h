#pragma once

#include <type_traits>
#include <cstdint>
#include "IComponent.h"

struct Entity {
public:
	Entity();
	~Entity();

	template <typename T>
	T* GetComponent();

	template <typename T>
	T* MakeComponent();

	template <typename T>
	void RemoveComponent();

protected:
	uint32_t myID;
	uint32_t myComponentBits;
};

template<typename T>
inline T * Entity::GetComponent() {
	if (myComponentBits & T::Mask) {
		return EntityManager::GetComponent<T>(myID);
		// todo: Lookup component
	}
	else {
		return nullptr;
	}
}

template<typename T>
inline T * Entity::MakeComponent()
{
	if (myComponentBits & T::Mask) {
		return GetComponent<T>();
	}
	else {
		T* result = EntityManager::CreateComponent<T>(myID);// TODO: create component;

		if (result != nullptr)
			myComponentBits |= T::Mask;

		return result;
	}
}

template<typename T>
inline void Entity::RemoveComponent()
{
	if (myComponentBits & T::Mask) {
		EntityManager::RemoveComponent<T>(myID);
	}
}
