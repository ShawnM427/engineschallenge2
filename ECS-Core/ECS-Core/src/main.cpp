/*
	Author: Shawn Matthews
	Date:   September 24, 2018
	Description:
		A test program to ensure functionality, only built in debug mode.
*/
#ifdef _DEBUG 

#include <cstdio>
#include <iostream>
#include <chrono>
#include <cstdint>

#include "Wrapper.h"
#include "Entity.h"
#include "EntityManager.h"

class Foo : public IComponent {
public:
	Foo() {}
	Foo(float x) {}

	float Doot;

	static uint32_t Mask;
};

uint32_t Foo::Mask = 0x00000001;

// Main entry point for the debugging progam
int main() {

	SetLogFileLoc("logs.txt");

	Entity e = Entity();
	e.MakeComponent<Foo>();

	{
		Foo* thing = e.GetComponent<Foo>();
		thing->Doot = 12.0f;
	}

	{
		Foo* thingy = e.GetComponent<Foo>();
		std::cout << thingy->Doot << std::endl;
	}

	e.RemoveComponent<Foo>();

	std::cout << e.GetComponent<Foo>() << std::endl;

	std::cin.get();

}

#endif //  DEBUG