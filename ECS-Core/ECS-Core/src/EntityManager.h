#pragma once

#include <cstdint>
#include <type_traits>
#include "IComponent.h"
#include "EntityComponentBinding.h"
#include "ComponentManager.h"

class EntityManager {
public:
	template <typename T>
	static T* GetComponent(uint32_t entity) {
		return ComponentManager<T>::Get(EntityComponentBinding<T>::GetBinding(entity));
	}

	template <typename T>
	static T* CreateComponent(uint32_t entity) {
		T* result = nullptr;
		uint32_t id = ComponentManager<T>::Create(result);
		EntityComponentBinding<T>::AddBinding(entity, id);
		return result;
	}

	template <typename T>
	static void RemoveComponent(uint32_t entity) {
		uint32_t componentId = EntityComponentBinding<T>::RemoveBinding(entity);
		ComponentManager<T>::Delete(componentId);
	}

protected:

};