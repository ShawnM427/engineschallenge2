#include "Wrapper.h"
#include "Logger.h"

// Implementation of set log file location
LIB_API void SetLogFileLoc(const char* fileName) {	
	// Try and open the file
	FILE* file = nullptr;
	fopen_s(&file, fileName, "w");

	// As long as the file opened
	if (file != nullptr) {
		// If there is a file already bound, close it
		if (Output2FILE::Stream() != nullptr) {
			fclose(Output2FILE::Stream());
			Output2FILE::Stream() = stderr;
		}
		// Bind the file
		Output2FILE::Stream() = file;
	}
}

LIB_API void NativeLog(int level, const char* text) {
	FILE_LOG((TLogLevel)level) << text;
}

