#pragma once

#include <cstdint>
#include <type_traits>

class IComponent {
public:
	virtual ~IComponent() {};

	virtual void Awake() {};
	virtual void Sleep() {};

	static uint32_t Mask;
};

#define IS_COMPONENT(T) typename std::enable_if<std::is_base_of<IComponent, T>::value>::type