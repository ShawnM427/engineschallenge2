#pragma once

#include <cstdint>
#include <unordered_map>
#include "IComponent.h"

template<class T>
class EntityComponentBinding {
public:
	static void AddBinding(uint32_t entityId, uint32_t componentId) {
		Map[entityId] = componentId;
	}

	static uint32_t GetBinding(uint32_t entityId) {
		return Map[entityId];
	}

	static uint32_t RemoveBinding(uint32_t entityId) {
		uint32_t result = Map[entityId];
		Map[entityId] = 0;
		return result;
	}

private:
	static std::unordered_map<uint32_t, uint32_t> Map;
};

template<class T>
std::unordered_map<uint32_t, uint32_t> EntityComponentBinding<T>::Map;

