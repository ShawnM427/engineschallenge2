/*
	Definitions for the exported functions
*/
#pragma once

#include "LibSettings.h"

#ifdef __cplusplus
extern "C" {
#endif
	/*
		Sets the file path for the logging output
		@param fileName The name of the file to save the logs to
	*/
	LIB_API void SetLogFileLoc(const char* fileName);
	/*
		Send text to the plugin's log file, at the given level
		@param level The LogLevel to send the message at
		@param text The text to send to the log
	*/
	LIB_API void NativeLog(int level, const char* text);


#ifdef __cplusplus
}
#endif